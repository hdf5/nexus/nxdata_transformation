# NXdata transformation

Documentation of the new *NXdata transformation*:

https://hdf5.gitlab-pages.esrf.fr/nexus/nxdata_transformation/classes/base_classes/NXdata.html#nxdata-transformation-field

Documentation of reference frames:

https://hdf5.gitlab-pages.esrf.fr/nexus/nxdata_transformation/notebooks/index.html

When ready we will submit it to the official *NeXus standard*:

https://github.com/nexusformat/definitions/pull/1033

